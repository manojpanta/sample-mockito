package com.shashi.test;

import com.shashi.entity.ProductEntity;
import com.shashi.vo.ProductVO;

public class ProductData {
	
	private ProductData() {
		
	}
	public static ProductVO create() {
		
		return new ProductVO(null, "HP", "hp dual core");
		
	}
	
	public static ProductEntity createEntity() {
		ProductEntity entity = new ProductEntity();
		entity.setDescription("hp dual core");
		entity.setProductName("HP");
		entity.setId(2000L);
		
		return entity;
	}
	

	

}
