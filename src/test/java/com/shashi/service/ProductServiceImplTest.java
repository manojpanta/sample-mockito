package com.shashi.service;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.shashi.dao.ProductDAO;
import com.shashi.dao.ProductDetailDAO;
import com.shashi.entity.ProductEntity;
import com.shashi.test.ProductData;
import com.shashi.vo.ProductVO;


@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {
	@Mock
	private ProductDAO dao;
	@Mock
	private ProductDetailDAO detailDao;
	
	private ProductServiceImpl impl = new ProductServiceImpl();

	@Test(expected = IllegalArgumentException.class)
	public void testNullSaveProduct() {
		impl.saveProduct(null);
	}
	@Test(expected = IllegalArgumentException.class)
	public void testSaveProductWithNullProductName() {
		impl.saveProduct(new ProductVO());
	}
	@Before
	public void setup() {
		impl.setDao(dao);
		impl.setDetailDao(detailDao);
	}
	
	@Test
	public void testCreateProduct() throws SQLException  {
		ProductEntity entity = ProductData.createEntity();
		Mockito.when(dao.createProduct(Mockito.any(ProductEntity.class))).thenReturn(entity);
		ProductVO input = ProductData.create();
		ProductVO result = impl.saveProduct(input);
		validateResult(input, result);
	}
	
	private void validateResult(ProductVO input, ProductVO result) {
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals(input.getProductName(), result.getProductName());
		assertEquals(input.getDescription(), result.getDescription());
	}
	
}
